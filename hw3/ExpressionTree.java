package hw3.student;

import java.text.ParseException;
import java.util.Stack;




public class ExpressionTree implements ExpressionTreeInterface {
	
    private ExpressionTreeNode root;
	/**
	 * Constructor to build an empty parse tree.
	 */


    public ExpressionTree() {
    	root = null;
    }
/**
   * Build a parse tree from an expression.
   * @param line String containing the expression
   *
   * @throws ParseException If an error occurs during parsing. 
   */


    public void parse(String line) throws ParseException {
    	Stack<String> initialStack = new Stack<>();	
        if (line.equals(null) == false) {       	
        	String[] elements = line.trim().split(" ");

            for (int i = elements.length - 1; i >= 0; i--) {
            	initialStack.push(elements[i]);
            }
            root = expressionToTree(initialStack);
        }
        else {
        	throw new ParseException("empty", 0);
        }       
    } 
    
	 //method for checking the precedence of the operators
    private static String checkOperator(Stack<String> stack) {          
             if (stack.peek().equals("+") || stack.peek().equals("-")) {
            	 return "+ -";
             }
             else if (stack.peek().equals("*") || stack.peek().equals("/")) {
            	 return "* /";
             }
             else {
            	 return "";
             }
    }
	//check if the operator is +/- then update the tree or throw the exception
    private static ExpressionTreeNode expressionToTree(Stack<String> stack) throws ParseException {
    	

    	ExpressionTreeNode leftNode = parseExpression(stack);
    	
    	String operator = "";
   	
    	 while (stack.empty() == false) {
    		 operator = checkOperator(stack);
    		 if (operator.equals("+ -")) {
				String item = stack.peek();
                stack.pop();
                ExpressionTreeNode rightNode = parseExpression(stack);
                leftNode = new ExpressionTreeNode(item, leftNode, rightNode); 
    		 }
    		 else {
                 return leftNode;
             }
         }
         return leftNode;
    } 
    //check if the operator is is valid number or operator the throw exception
    private static ExpressionTreeNode parseExpression(Stack<String> stack) throws ParseException {
    	
    	ExpressionTreeNode leftNode = parseTerms(stack);
    	
    	String operator = "";

        if (stack.empty() == false && checkInteger(stack.peek())) {
            throw new ParseException("too many integers", 0);
        }
        else if (stack.empty() == false && checkOperator(stack) == "" && stack.peek().equals(")") == false) {
            throw new ParseException("invalid operator", 0);
        }

        while (stack.empty() == false) {
            operator = checkOperator(stack);
            if (operator.equals("* /")) {
            	String item = stack.peek();
                stack.pop();
                ExpressionTreeNode rightNode = parseTerms(stack);
                leftNode = new ExpressionTreeNode(item, leftNode, rightNode);
            } 
            else {
                return leftNode;
            }
        }
        return leftNode;
    } 
 	// check if the stack is empty or not the throw the exception
    private static ExpressionTreeNode parseTerms(Stack<String> stack) throws ParseException {
    	
    	ExpressionTreeNode node;
  	
        if (stack.empty()) {
            throw new ParseException("empty stack", 0);
        }

        String item = stack.peek();
        if (stack.peek().equals("(") || stack.peek().equals(")")) {
            stack.pop();
            node = expressionToTree(stack);
            if (stack.empty()) {
                throw new ParseException("empty", 0);
            }
            else if (stack.peek().equals(")") == false) {
                throw new ParseException("missing parenthesis", 0);
            }
            stack.pop();
        } 
        else {
        	if (checkInteger(item) == true) {
        		node = new ExpressionTreeNode(stack.pop());
        	}
        	else {
                throw new ParseException("invalid number", 0);
            }          
        }
        return node;
    } 
   
	/**
   * Evaluate the expression tree and return the integer result. An 
   * empty tree returns 0.
   */
    public int evaluate() {
        return evaluateExpression(root);
    } 

    private int evaluateExpression(ExpressionTreeNode node) {
    	String operator = node.el;
        if (isLeafNode(node) == true) {
            return Integer.parseInt(node.el);
        } 
        else if (operator.equals("*")) {
            return evaluateExpression(node.left) * evaluateExpression(node.right);
        } 
        else if (operator.equals("/") && node.right.equals("0") == false) {
            return evaluateExpression(node.left) / evaluateExpression(node.right);
        } 
        else if (operator.equals("+")) {
            return evaluateExpression(node.left) + evaluateExpression(node.right);
        } 
        else {
            return evaluateExpression(node.left) - evaluateExpression(node.right);
        } 
    } 
    
/**
   * Simplifies the tree to a specified height h (h >= 0). After simplification, the tree 
   * has a height of h. Any subtree rooted at a height of h is replaced by a leaf node 
   * containing the evaluated result of that subtree.  
   * If the height of the tree is already less than the specified height, the tree is unaffected. 
   * 
   * @param h The height to simplify the tree to.
   */

    public void simplify( int h ) {
        simplifyeval(root, h, 0);
    }
    public void simplifyeval(ExpressionTreeNode n, int h, int a) {
        if(n == null) {
            return;
        }
        if(h == a) {
            n.el = "" + evaluateExpression(n);
            n.right = null;
            n.left = null;
        }
        else {
            simplifyeval(n.left, h, a +1);
            simplifyeval(n.right, h, a+1);
        }
    }
    //update the tree height
    private int treeHeight(ExpressionTreeNode node) {
    	if (node == null) {
    		return 0;
    	}    
        else{ 
			 int lheight = treeHeight(node.left); 
			 int rheight = treeHeight(node.right); 
			   
			 if (lheight > rheight) {
			   return(lheight+1);
			 }
			 else {
			   return(rheight+1);  			           
			 } 
         }     	
    }
    
	 /**
   * Returns a parentheses-free postfix representation of the expression. Tokens 
   * are separated by whitespace. An empty tree returns a zero length string.
   */
    public String postfix() {
    	String postOrderExpression = postOrder(root, "");
        return postOrderExpression;
    } 
	//return the postorder of the tree
    private String postOrder(ExpressionTreeNode node, String expression) {
        if (node != null) {
        	expression = postOrder(node.left, expression);
        	expression = postOrder(node.right, expression);
        	expression = expression.concat(node.el + " ");
        }
        return expression;
    }

 /**
   * Returns a parentheses-free prefix representation of the expression. Tokens are 
   * separated by whitespace. An empty tree returns a zero length string.
   */
    public String prefix() {
    	String preOrderExpression = preOrder(root, "");
        return preOrderExpression;
    } 

	//return the preorder of the tree
    private String preOrder(ExpressionTreeNode node, String expression) {
        if (node != null) {
        	expression = expression.concat(node.el + " ");
        	expression = preOrder(node.left, expression);
        	expression = preOrder(node.right, expression);
        }
        return expression;
    } 
	//check if the data is int or not
    private static boolean checkInteger(String s) {
        boolean isInt = false;
        try {
            Integer.parseInt(s);
            isInt = true;
        } 
        catch (NumberFormatException e) {
        	e.printStackTrace();
        }
        return isInt;
    } 
    //check if the node is empty or not
    public boolean isLeafNode(ExpressionTreeNode node) {
    	if (node.left == null && node.right == null) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    public static void main(String args[]) {
    	ExpressionTree test = new ExpressionTree();
    	try {
			test.parse("( 9 - 5 ) * 2");    //test case 
			test.evaluate();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
  
}
