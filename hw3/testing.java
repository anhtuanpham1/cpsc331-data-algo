package hw3.student;

import java.text.ParseException;

public class testing {

	public static void main(String[] args) throws ParseException {
		ExpressionTree numbuh1 = new ExpressionTree();
		numbuh1.parse("( 2 + 2 * ( 7 + 2 * ( 11 * 2 - 4 ) ) ) / ( 1 + ( 2 - 3 ) * 2 )");
		numbuh1.printTree();
		System.out.printf("\n%d", numbuh1.evaluate());
		numbuh1.simplify(4);
		System.out.printf("\n%s",numbuh1.postfix());
		
	}

}
